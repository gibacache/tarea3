/**********************************************
 * IIC2333 - Cliente simple de servidor
 **********************************************/

// Incluimos lo necesario

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

/**
 * Funcion para errores
 */
void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int write_ack(int sockfd, char *buffer, size_t size) {
    char ok[1] = {'\0'};
    int n = write(sockfd, buffer, size);
    read(sockfd, ok, 1);
    return n;
}


void command_put(char *buffer, char *token, int sockfd) {
    int n;
    // Escribir PUT
    n = write(sockfd, token, strlen(token) + 1);
    //Leer nombre
    printf("C: ");
    bzero(buffer, 500);
    fgets(buffer, 500, stdin);
    token = strtok(buffer, "\n");
    char *filename = strdup(token + 6);
    // Leer archivo
    char command[500];
    sprintf(command, "./%s", filename);
    FILE *fp = fopen(command, "r");
    if (fp == NULL) {
        //EScribir error
        token = strdup("NAME: FAIL");
        n = write_ack(sockfd, token, strlen(token) + 1);
        printf("C: File %s doesn't exist in the current folder\n", filename);
    } else {
        //EScribir NAME
        n = write_ack(sockfd, token, strlen(token) + 1);
        // Leer largo
        bzero(buffer, 500);
        fseek(fp, 0, SEEK_END); // seek to end of file
        int length = ftell(fp); // get current file pointer
        fseek(fp, 0, SEEK_SET); // seek back to beginning of file
        printf("C: LENGTH: %d\n", length);
        sprintf(buffer, "LENGTH: %d\n", length);
        n = write_ack(sockfd, buffer, strlen(buffer) + 1);
        bzero(buffer, 500);
        while (fgets( buffer, 500, fp ) != NULL) {
            printf("C: %s", buffer);
            n = write_ack(sockfd, buffer, strlen(buffer) + 1);
        }
        fclose(fp);
    }
    printf("C: END\n");
    bzero(buffer, 500);
    sprintf(buffer, "END\n");
    token = strtok(buffer, "\n");
}

/**
 * Funcion principal
 * Se llama especificando el host y puerto
 */
int main(int argc, char *argv[])
{
    // Definimos las estructuras a ocupar
    int sockfd, port_number, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    // Controlamos el error que no se hayan especificado los datos necesario (phost y puerto)
    char buffer[500];
    if (argc < 3) {
        fprintf(stderr, "Usage %s hostname port\n", argv[0]);
        exit(0);
    }

    // Extraemos el puerto
    port_number = atoi(argv[2]);

    // Creamos el socket, controlando el error si es que no puede ser creado
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error("ERROR opening socket");

    // Obtenemos el servidor para ver si es valido
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr, "ERROR, no such host\n");
        exit(0);
    }

    // Obtenemos los datos para conectarnos al servidor
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
          (char *)&serv_addr.sin_addr.s_addr,
          server->h_length);
    serv_addr.sin_port = htons(port_number);

    // Intentamos conectarnos al servidor
    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        error("ERROR connecting");
    int close_connection = 0;
    while (!close_connection) {
        int continue_write = 1;
        while (continue_write) {
            //Solicitamos el mensaje a enviar
            printf("C: ");
            bzero(buffer, 500);
            fgets(buffer, 500, stdin);
            char *token;
            /* get the first token */
            token = strtok(buffer, "\n");
            while (token != NULL) {
                if (!strcmp(token, "CLOSE"))
                    close_connection = 1;
                else if (!strcmp(token, "END"))
                    continue_write = 0;
                else if (!strcmp(token, "PUT")) {
                    command_put(buffer, token, sockfd);
                    continue_write = 0;
                }
                // Lo escribimos intentamos enviar
                n = write(sockfd, token, strlen(token) + 1);
                if (n < 0)
                    error("ERROR writing to socket");
                token = strtok(NULL, "\n");
            }
        }
        // Obtenemos la respuesta
        int continue_read = 1;
        printf("\n");
        while (continue_read) {
            bzero(buffer, 500);
            n = read(sockfd, buffer, 500);
            if (n < 0)
                error("ERROR reading from socket");
            char *token;
            /* get the first token */
            token = strtok(buffer, "\n");
            while (token != NULL) {
                if (!strcmp(token, "END"))
                    continue_read = 0;
                // Mostramos la respuesta en pantalla
                printf("S: %s\n", token);
                token = strtok(NULL, "\n");
            }
        }
        printf("\n");
    }
    // Cerramos el descriptor
    close(sockfd);
    return 0;
}
