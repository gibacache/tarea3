/*********************************************/
/* IIC2333 Ejemplo de un servidor simple     */
/*********************************************/

// Incluimos lo necesario

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pwd.h>
#include <grp.h>
#include <sys/stat.h>

/**
 * Funcion para desplegar los errores
 */
void error(const char *msg)
{
	perror(msg);
	exit(1);
}


int read_ack(int client_socket_descriptor, char *buffer, size_t size) {
	char ok[1] = {'\0'};
	int n = read(client_socket_descriptor, buffer, size);
	write(client_socket_descriptor, ok, 1);
	return n;
}

typedef struct FileInfo {
	char *filename;
	char *shared;
	char *owner;
} FileInfo;

// Definimos estructuras necesarias
int server_socket_descriptor, client_socket_descriptor, port_number;
socklen_t client_lenght;
char buffer[500];
struct sockaddr_in server_addr, client_addr;
FILE *config;
int n;

FileInfo *file_info_init(char *filename, int shared, char *owner) {
	FileInfo *file_info = malloc(sizeof(FileInfo));
	file_info->filename = strdup(filename);
	file_info->owner = strdup(owner);
	if (shared)
		file_info->shared = strdup("shared");
	else
		file_info->shared = strdup("notshared");
	return file_info;
}

void file_info_destroy(FileInfo *file_info) {
	free(file_info->filename);
	free(file_info->shared);
	free(file_info->owner);
	free(file_info);
}


FileInfo *parse_file(char* str) {
	char *permissions, *links, *owner, *group, *size, *month, *day, *time, *filename;
	permissions = strtok (str, " ");
	links = strtok (NULL, " ");
	owner = strtok (NULL, " ");
	group = strtok (NULL, " ");
	size = strtok (NULL, " ");
	month = strtok (NULL, " ");
	day = strtok (NULL, " ");
	time = strtok (NULL, " ");
	filename = strtok (NULL, " \n");
	int shared = permissions[7] == 'r' ? 1 : 0;
	return file_info_init(filename, shared, owner);
}

char *user;

int is_shared(char *filename) {
	FILE *fp;
	char command[500];
	char path[1035];
	sprintf(command, "ls -lh ./Files/%s", filename);
	fp = popen(command, "r");
	fgets(path, sizeof(path) - 1, fp);
	fgets(path, sizeof(path) - 1, fp);
	pclose(fp);
	FileInfo *file_info = parse_file(path);
	char str[256];
	if (!strcmp(file_info->shared, "shared")) {
		file_info_destroy(file_info);
		return 1;
	}
	else {
		file_info_destroy(file_info);
		return 0;
	}
}

int file_exists(char *filename) {
	FILE *fp;
	char command[500];
	sprintf(command, "./Files/%s", filename);
	fp = fopen(command, "r");
	if (fp == NULL) {
		return 0;
	}	else {
		fclose(fp);
		return 1;
	}
}

int is_owner(char *filename) {
	FILE *fp;
	char path[1035];
	char command[500];
	sprintf(command, "ls -lh ./Files/%s", filename);
	fp = popen(command, "r");
	fgets(path, sizeof(path) - 1, fp);
	fgets(path, sizeof(path) - 1, fp);
	pclose(fp);
	FileInfo *file_info = parse_file(path);
	if (user != NULL && !strcmp(file_info->owner, user)) {
		file_info_destroy(file_info);
		return 1;
	}
	else {
		file_info_destroy(file_info);
		return 0;
	}
}

char *get_owner(char *filename) {
	FILE *fp;
	char path[1035];
	char command[500];
	sprintf(command, "ls -lh ./Files/%s", filename);
	fp = popen(command, "r");
	fgets(path, sizeof(path) - 1, fp);
	fgets(path, sizeof(path) - 1, fp);
	pclose(fp);
	FileInfo *file_info = parse_file(path);
	char *owner = strdup(file_info->owner);
	file_info_destroy(file_info);
	return	owner;
}

void command_ls(char *ans) {
	FILE *fp;
	fp = popen("/bin/ls -lh ./Files", "r");
	if (fp == NULL) {
		printf("Failed to run command\n" );
		exit(1);
	}
	strcat(ans, "OK\n");
	strcat(ans, "Message: Repository list\n");
	char path[1035];
	fgets(path, sizeof(path) - 1, fp);
	while (fgets(path, sizeof(path) - 1, fp) != NULL)
	{
		FileInfo *file_info = parse_file(path);
		char line[255];
		sprintf(line, "%s;%s;%s\n", file_info->filename, file_info->owner, file_info->shared);
		strcat(ans, line);
		file_info_destroy(file_info);
	}
	pclose(fp);
}

void command_rm(char *ans, char *filename) {

	FILE *fp;
	char str[256];
	char command[500];
	if (!file_exists(filename)) {
		sprintf(str, "Message: File %s doesn't exist\n", filename);
		strcat(ans, "FAIL\n");
		strcat(ans, str);
	} else {
		if (is_owner(filename)) {
			sprintf(command, "rm ./Files/%s", filename);
			popen(command, "r");
			strcat(ans, "OK\n");
			sprintf(str, "Message: File %s deleted\n", filename);
			strcat(ans, str);
		} else {
			strcat(ans, "FAIL\n");
			sprintf(str, "Message: User %s doesn't own file %s\n", user, filename);
			strcat(ans, str);
		}
	}
}

void command_share(char *ans, char *filename) {
	FILE *fp;
	char command[500];
	char str[256];
	if (!file_exists(filename)) {
		sprintf(str, "Message: File %s doesn't exist\n", filename);
		strcat(ans, "FAIL\n");
		strcat(ans, str);
	} else {
		if (is_owner(filename)) {
			if (is_shared(filename)) {
				sprintf(command, "chmod o-r ./Files/%s", filename);
				popen(command, "r");
				strcat(ans, "OK\n");
				sprintf(str, "Message: File %s not shared by %s\n", filename, user);
				strcat(ans, str);
			} else {
				sprintf(command, "chmod o+r ./Files/%s", filename);
				popen(command, "r");
				strcat(ans, "OK\n");
				sprintf(str, "Message: File %s shared by %s\n", filename, user);
				strcat(ans, str);
			}
		} else {
			strcat(ans, "FAIL\n");
			sprintf(str, "Message: User %s doesn't own file %s\n", user, filename);
			strcat(ans, str);
		}
	}
}

void command_user(char *ans, char *user_name) {
	user = strdup(user_name);
	strcat(ans, "OK\n");
	char str[256];
	sprintf(str, "Message: User identified as %s\n", user);
	strcat(ans, str);
}

void command_get(char *ans, char *filename) {
	char str[256];
	if (!file_exists(filename)) {
		strcat(ans, "FAIL\n");
		sprintf(str, "Message: File %s doesn't exist\n", filename);
		strcat(ans, str);
	} else {
		if (is_owner(filename) || is_shared(filename)) {
			FILE *fp;
			char command[500];
			sprintf(command, "./Files/%s", filename);
			fp = fopen(command, "r");
			int length;
			fseek(fp, 0, SEEK_END); // seek to end of file
			length = ftell(fp); // get current file pointer
			fseek(fp, 0, SEEK_SET); // seek back to beginning of file
			strcat(ans, "OK\n");
			sprintf(str, "Length: %d\n", length);
			strcat(ans, str);
			n = write(client_socket_descriptor, ans, strlen(ans));
			char ans2[500];
			while (fgets( ans2, 500, fp ) != NULL) {
				n = write(client_socket_descriptor, ans2, strlen(ans2));
			}
			bzero(ans, 500);
		} else {
			strcat(ans, "FAIL\n");
			sprintf(str, "Message: File %s is owned by %s and not shared\n", filename, get_owner(filename));
			strcat(ans, str);
		}
	}
}

void command_put(char *ans, char *filename, char *buffer, int client_socket_descriptor) {
	char str[255];
	if (strcmp(filename, "FAIL")) {
		n = read_ack(client_socket_descriptor, buffer, 500);
		int length = atoi(buffer + 8);
		FILE *fp;
		char command[500];
		sprintf(command, "./Files/%s", filename);
		fp = fopen(command, "w");
		int bytes_wrote = 0;
		while (bytes_wrote < length) {
			n = read_ack(client_socket_descriptor, buffer, 500);
			n = n < 500 ? n - 1 : n;
			int i;
			for (i = 0; i < n && bytes_wrote < length ; i++, bytes_wrote++)
				fputc(buffer[i], fp);
			printf("C: %s\n", buffer);
		}
		fclose(fp);
		strcat(ans, "OK\n");
		sprintf(str, "Message: File %s saved\n", filename);
		strcat(ans, str);
	}	else {
		strcat(ans, "FAIL\n");
		sprintf(str, "Message: PUT aborted\n");
		strcat(ans, str);
	}
}


/**
 * Funcion principal, se llama indicando el puerto de escucha
 */
int main(int argc, char *argv[])
{

	// Lee el puerto desde el .conf
	config = fopen ("fileserver.conf", "r");
	fscanf(config, "%d", &port_number);
	fclose(config);

	// Creamos el socket del servidor
	server_socket_descriptor = socket(AF_INET, SOCK_STREAM, 0);

	// Controlamos el error de que no haya podido ser creado
	if (server_socket_descriptor < 0)
		error("ERROR opening socket");

	// Especificamos los detalles para el servidor
	bzero((char *) &server_addr, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = INADDR_ANY;
	server_addr.sin_port = htons(port_number);

	// Registramos el socket
	if (bind(server_socket_descriptor, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0)
		error("ERROR on binding");

	// Comenzamos a escuchar por el socket
	listen(server_socket_descriptor, 5);
	printf("-- Server listening on Port %d --\n", port_number);

	// Obtenemos el largo del mensaje
	client_lenght = sizeof(client_addr);

	while (1) {
		// Aceptamos la conexion de algun cliente
		client_socket_descriptor = accept(server_socket_descriptor,
		                                  (struct sockaddr *) &client_addr,
		                                  &client_lenght);

		// Si no se puede obtener el descriptor, enviamos un error
		if (client_socket_descriptor < 0)
			error("ERROR on accept");

		// Buffer para la respuesta
		char ans[500];
		int keep_going = 1;
		while (keep_going) {
			bzero(ans, 500);
			// Comenzamos la lectura del mensaje
			bzero(buffer, 500);
			n = read(client_socket_descriptor, buffer, 500);

			// Controlamos el error de lectura del socket
			if (n < 0)
				error("ERROR reading from socket");

			// Impimimos el mensaje recibido
			printf("C: %s\n", buffer);

			// Seleccion de comando
			if (!strcmp(buffer, "USER")) {

				n = read(client_socket_descriptor, buffer, 500);
				char *user_name = buffer + 6;
				command_user(ans, user_name);

			} else if (!strcmp(buffer, "GET")) {

				n = read(client_socket_descriptor, buffer, 500);
				char *filename = buffer + 6;
				command_get(ans, filename);

			} else if (!strcmp(buffer, "PUT")) {

				n = read_ack(client_socket_descriptor, buffer, 500);
				char *filename = strdup(buffer + 6);
				command_put(ans, filename, buffer, client_socket_descriptor);

			} else if (!strcmp(buffer, "LS")) {

				command_ls(ans);

			} else if (!strcmp(buffer, "RM")) {

				n = read(client_socket_descriptor, buffer, 500);
				char *filename = buffer + 6;
				command_rm(ans, filename);

			} else if (!strcmp(buffer, "SHARE")) {

				n = read(client_socket_descriptor, buffer, 500);
				char *filename = buffer + 6;
				command_share(ans, filename);

			} else if (!strcmp(buffer, "CLOSE")) {

				strcat(ans, "OK\nMessage: BYE\n");
				keep_going = 0;

			} else {

				strcat(ans, "FAIL\n");
				char str[255];
				sprintf(str, "Message: Command %s not found\n", buffer);
				strcat(ans, str);

			}
			strcat(ans, "END\n");
			while (1) {
				n = read(client_socket_descriptor, buffer, 500);
				if (!strcmp(buffer, "END")) {
					printf("C: %s\n", buffer );
					break;
				}
			}
			// Mostramos la respuesta en pantalla
			printf("\n%s\n", ans);
			// Enviamos confirmacion al cliente
			n = write(client_socket_descriptor, ans, strlen(ans));
			// Si no se pudo escribir, enviamos un error
			if (n < 0)
				error("ERROR writing to socket");
		}
		// Cerramos los descriptores
		close(client_socket_descriptor);
	}
	close(server_socket_descriptor);

	return 0;
}
