tarea3: client server

all: tarea3

client:
	$(CC) simple_client.c -o simpleClient

server:
	$(CC) simple_server.c -o simpleServer

clean:
	rm -f simpleServer
	rm -f simpleClient
